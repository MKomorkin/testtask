//
//  SongModel.swift
//  TestTask
//
//  Created by КМХ on 11.03.2019.
//  Copyright © 2019 КМХ. All rights reserved.
//
import Foundation

class SongModel {
    let songName: String
    let artistName: String
    let albumCoverUrl: String
    let songPreviewUrlString: String
    
    init(_ song: String, _ artist: String, _ cover: String, _ previewUrl: String) {
        self.songName = song
        self.artistName = artist
        self.albumCoverUrl = cover
        self.songPreviewUrlString = previewUrl
    }
}
