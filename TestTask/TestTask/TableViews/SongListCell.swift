//
//  SongListCell.swift
//  TestTask
//
//  Created by КМХ on 11.03.2019.
//  Copyright © 2019 КМХ. All rights reserved.
//

import UIKit

class SongListCell: UITableViewCell {
    @IBOutlet weak var albumCover: UIImageView!
    @IBOutlet weak var song: UILabel!
    @IBOutlet weak var artist: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
