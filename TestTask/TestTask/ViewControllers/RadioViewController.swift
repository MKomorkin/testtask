//
//  RadioViewController.swift
//  TestTask
//
//  Created by КМХ on 12.03.2019.
//  Copyright © 2019 КМХ. All rights reserved.
//

import UIKit
import AVFoundation

class RadioViewController: UIViewController, URLSessionDownloadDelegate {
    @IBOutlet weak var albumCoverImageView: UIImageView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    var playIndicator: Bool = false
    
    var albumCoverImage: UIImage!
    var songName: String!
    var artistName: String!
    
    var downloadTask:URLSessionDownloadTask!
    var backgroundSession: URLSession!
    var songPreviewUrlString: String!
    var savedPreviewUrl: URL!
    
    var player: AVAudioPlayer?
    
    fileprivate func loadSongInfo() {
        albumCoverImageView.image = albumCoverImage
        songNameLabel.text = songName
        artistNameLabel.text = artistName
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSongInfo()
        
        //Disable play button unless the track is fully downloaded
        if #available(iOS 10.0, *) {
            try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .defaultToSpeaker)
        } else {
            AVAudioSession.sharedInstance().perform(NSSelectorFromString("setCategory:error:"), with: AVAudioSession.Category.playback)
        }
        
        downloadFileFromURL(urlString: songPreviewUrlString)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.player?.stop()
        self.backgroundSession.finishTasksAndInvalidate()
    }
    
    @IBAction func playPauseToggle(_ sender: Any) {
        playIndicator.toggle()
        if playIndicator {
            playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
            self.player?.play()
        } else {
            playPauseButton.setImage(UIImage(named: "play"), for: .normal)
            self.player?.pause()
            
        }
    }
    
    @IBAction func stopPressed(_ sender: Any) {
        if playIndicator {
            playIndicator.toggle()
            playPauseButton.setImage(UIImage(named: "play"), for: .normal)
            self.player?.play()
        }
        self.player?.stop()
        self.player?.currentTime = 0
    }
    
    func downloadFileFromURL(urlString:String){
        guard let downloadUrl = URL(string: urlString) else { return }
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        let downloadTask = backgroundSession.downloadTask(with: downloadUrl)
        print("Starting download at \(urlString)")
        downloadTask.resume()
        
    }
    
    fileprivate func openFileInAudioPlayer(_ location: URL) {
        do {
            self.player = try AVAudioPlayer(contentsOf: location)
            self.player?.prepareToPlay()
            print("Ready to play")
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("Downloaded file from \(String(describing: self.songPreviewUrlString)): \(location)")
        openFileInAudioPlayer(location)
        self.playPauseButton.isEnabled = true
    }
}
