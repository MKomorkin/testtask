//
//  ViewController.swift
//  TestTask
//
//  Created by КМХ on 11.03.2019.
//  Copyright © 2019 КМХ. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    static let SEARCH_BASE = "https://itunes.apple.com/search"
    
    @IBOutlet weak var searchLine: UISearchBar!
    @IBOutlet weak var songTableView: UITableView!
    
    var songArray = [SongModel]()
    
    var indicator = UIActivityIndicatorView()
    var currentCellHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchLine.returnKeyType = UIReturnKeyType.done
        self.navigationItem.titleView = searchLine
        setupActivityIndicator()
        
        setCurrentCellHeight()
    }
    
    fileprivate func setCurrentCellHeight() {
        // Set initial cell Height based on screen size.
        // Check orientation  in order to keep the same cell height in both cases.
        if UIDevice.current.orientation.isLandscape {
            currentCellHeight = self.view.frame.width / 7
            songTableView.reloadData()
        } else {
            currentCellHeight = self.view.frame.height / 7
            songTableView.reloadData()
        }
    }

    func setupActivityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        indicator.style = .gray
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    func searchForSong(_ searchString: String) {
        var fullUrl = URLComponents(string: ViewController.SEARCH_BASE)!
        fullUrl.queryItems = [
            URLQueryItem(name: "term", value: searchString)
        ]
        guard let search = fullUrl.url else { return }
        print("Calling Itunes API: \(search)")
        getData(from: search) { data, response, error in
            
            if error != nil {
                print("Get Error")
            }else{
                guard let data = data else { return }
                do {
                    guard let jsonResponse:NSDictionary =  try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? NSDictionary else { return }
                    print("jsonResponse:\n\n\(jsonResponse)")
                    
                    var currentSearchArray = [SongModel]()
                    if let resultList:[NSDictionary] = jsonResponse.object(forKey: "results") as? [NSDictionary] {
                        for result:NSDictionary in resultList {
                            if let foundArtist = result.object(forKey: "artistName"),
                                let foundSong = result.object(forKey: "trackName"),
                                let foundCoverUrl = result.object(forKey: "artworkUrl100"),
                                let foundPreviewUrl = result.object(forKey: "previewUrl") {
                                currentSearchArray.append(SongModel(foundSong as! String, foundArtist as! String, foundCoverUrl as! String, foundPreviewUrl as! String))
                                
                            }
                        }
                        DispatchQueue.main.async {
                            self.indicator.stopAnimating()
                            self.indicator.hidesWhenStopped = true
                            self.songArray = currentSearchArray
                            self.songTableView.reloadData()
                        }
                    }
                    
                } catch let error as NSError {
                    // error handling
                    print(error.localizedDescription)
                }
            }
        }
        //task.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlayerScreenVC" {
            if let destination = segue.destination as? RadioViewController {
                guard let indexPath = self.songTableView.indexPathForSelectedRow else { return }
                if let chosenCell:SongListCell = songTableView.cellForRow(at: indexPath) as? SongListCell {
                    destination.albumCoverImage = chosenCell.albumCover.image
                    destination.artistName = chosenCell.artist.text
                    destination.songName = chosenCell.song.text
                    destination.songPreviewUrlString = songArray[indexPath.row].songPreviewUrlString
                }
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }


}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return currentCellHeight
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = songTableView.dequeueReusableCell(withIdentifier: "cell") as? SongListCell else {
            return UITableViewCell()
        }
        
        cell.artist.text = songArray[indexPath.row].artistName
        cell.song.text = songArray[indexPath.row].songName
        cell.albumCover.imageFromServerURL(songArray[indexPath.row].albumCoverUrl, placeHolder: UIImage())
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "PlayerScreenVC", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload(_:)), object: searchBar)
        indicator.startAnimating()
        indicator.backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.1)
        if searchText.count > 4 {
            perform(#selector(self.reload(_:)), with: searchBar, afterDelay: 0.4)
        } else {
            indicator.stopAnimating()
            indicator.hidesWhenStopped = true
            songArray = []
            songTableView.reloadData()
        }
    }

    @objc func reload(_ searchBar: UISearchBar) {
        guard let query = searchBar.text, query.replacingOccurrences(of: " ", with: "+") != "" else {
            print("nothing to search")
            indicator.stopAnimating()
            indicator.hidesWhenStopped = true
            songArray = []
            songTableView.reloadData()
            return
        }
        
        let searchLine = query.replacingOccurrences(of: " ", with: "+")
        searchForSong(searchLine)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
